--����� ��: https://docs.google.com/document/d/1NVORWgdwlKepKq_b8SPRaSpraltxoMg2SIusTEN6mEQ/edit?usp=sharing
--colab/jupyter: https://colab.research.google.com/drive/1j4XdGIU__NYPVpv74vQa9HUOAkxsgUez?usp=sharing

--task1 (lesson5)
-- ������������ �����: ������� view (pages_all_products), � ������� ����� ������������ �������� ���� ��������� (�� ����� ���� ��������� �� ����� ��������). �����: ��� ������ �� laptop, ����� ��������, ������ ���� �������
create view pages_all_products as
(
select *,
	  CASE WHEN num % 2 = 0 THEN num/2 ELSE num/2 + 1 END AS page_num, 
	  CASE WHEN total % 2 = 0 THEN total/2 ELSE total/2 + 1 END AS num_of_pages
from (
SELECT row_number() over(ORDER BY model, maker) num,
	COUNT(*) OVER() AS total,
model, price, code, maker, type
from
(
select maker, p1.model, p1.type, code, price 
from product p1
join printer p2
on p1.model=p2.model
union
select maker, p1.model, p1.type, code, price
from product p1
join pc p2
on p1.model=p2.model
union 
select maker, p1.model, p1.type, code, price
from product p1
join laptop p2
on p1.model=p2.model
)a1
)a2
where type = 'Laptop'
)

--task2 (lesson5)
-- ������������ �����: ������� view (distribution_by_type), � ������ �������� ����� ���������� ����������� ���� ������� �� ���� ����������. �����: �������������,

create view distribution_by_type as
(
select maker,type, count(type)/sum(count (type)) over (partition by type)*100 as type_upd
from product
group by maker, type
)



--task3 (lesson5)
-- ������������ �����: ������� �� ���� ����������� view ������ - �������� ���������
df = pd.read_sql_query("select * from distribution_by_type", conn)
fig = px.pie(df['type'].to_list(), df.maker.to_list(), df.type_upd.to_list(), labels={'type_upd'})
fig.show()

--task4 (lesson5)
-- �������: ������� ����� ������� ships (ships_two_words), �� � �������� ������� ������ �������� �� ���� ����

CREATE TABLE ships_two_words AS
SELECT *
FROM ships
WHERE name LIKE '% %' 

--task5 (lesson5)
-- �������: ������� ������ ��������, � ������� class ����������� (IS NULL) � �������� ���������� � ����� "S"
select *
from Outcomes o 
full join ships s
on o.ship = s.name
where class is null and ship LIKE 'S%'

--task6 (lesson5)
-- ������������ �����: ������� ��� �������� ������������� = 'A' �� ���������� ���� ������� �� ��������� ������������� = 'C' � ��� ����� ������� (����� ������� �������). ������� model

select model
from
((select p.model
from printer pr
join product p
on pr.model=p.model
where maker = 'A' and price > (select avg(price) from printer pr
                               join product p
                               on pr.model=p.model
                               where maker = 'C' is not null  ))
union
(select model from( 
select model,row_number() over (order by price desc) as rn
from printer)a
where rn<4)
)aa